package com.example.ThreadSpringBoot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ThreadSpringBoot.model.Country;
import com.example.ThreadSpringBoot.model.Customer;
import com.example.ThreadSpringBoot.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	 @Autowired
	 CustomerService customerService;

	 @GetMapping("/{id}")
	 public CompletableFuture < Customer > getCustomerById(@PathVariable String id) throws InterruptedException {
		 return customerService.getCustomerByID(id);
	 }

	 @GetMapping("/{id}/{email}")
	 public Customer getCustomerByEmail(@PathVariable String email) throws InterruptedException {
		 return customerService.getCustomerByEmail(email);
	 }
	 
	 @GetMapping("/country/{id}")
	 public List<String> getAllEuropeanFrenchSpeakingCountries() throws Throwable {
		 CompletableFuture<List<Country>> countriesByLanguageFuture = customerService.getCountriesByLanguage("fr");
		 CompletableFuture<List<Country>> countriesByRegionFuture = customerService.getCountriesByRegion("europe");
		 List<String> europeanFrenchSpeakingCountries;
		 try {
			 europeanFrenchSpeakingCountries = new ArrayList<>(countriesByLanguageFuture.get().stream().map(Country::getName).collect(Collectors.toList()));
			 europeanFrenchSpeakingCountries.retainAll(countriesByRegionFuture.get().stream().map(Country::getName).collect(Collectors.toList()));
		 } catch (Throwable e) {
			 throw e.getCause();
		 }

		 return europeanFrenchSpeakingCountries;
	 }
}
