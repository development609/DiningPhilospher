package com.example.ThreadSpringBoot.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ThreadSpringBoot.AsyncTaskDemo;

@RestController
public class DemoController {

	@Autowired
	private AsyncTaskDemo asyncDemo;

	@RequestMapping("/hello")
	public Map<String, String> callAsyncMethod() {

		asyncDemo.someAsyncMethod();
		
		return new HashMap<String, String>();  // returns empty braces
	}
}	
