package com.example.ThreadSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ThreadSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThreadSpringBootApplication.class, args);
	}

}
