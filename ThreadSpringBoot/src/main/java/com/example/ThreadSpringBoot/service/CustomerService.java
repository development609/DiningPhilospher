package com.example.ThreadSpringBoot.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.example.ThreadSpringBoot.model.Country;
import com.example.ThreadSpringBoot.model.Customer;

public interface CustomerService {
	
	public CompletableFuture<Customer> getCustomerByID(final String id) throws InterruptedException;
	
	public Customer getCustomerByEmail(String email) throws InterruptedException;

	CompletableFuture<List<Country>> getCountriesByLanguage(String language);

	CompletableFuture<List<Country>> getCountriesByRegion(String region);

}
