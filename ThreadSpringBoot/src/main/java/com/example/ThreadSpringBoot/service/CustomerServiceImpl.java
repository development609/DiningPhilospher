package com.example.ThreadSpringBoot.service;


import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.ThreadSpringBoot.model.Country;
import com.example.ThreadSpringBoot.model.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger LOG = LoggerFactory.getLogger(CustomerServiceImpl.class);

	RestTemplate restTemplate = new RestTemplate();
	
	 @Override
	 @Async	
	 public CompletableFuture<Customer> getCustomerByID(final String id) throws InterruptedException {
		  LOG.info("Filling the customer details for id {} ", id);
		  Customer customer = new Customer();
		  customer.setFirstName("Java");
		  customer.setLastName("Thread");
		  customer.setAge(34);
		  customer.setEmail("contact-us@javathread");
		  Thread.sleep(20000);
		  return CompletableFuture.completedFuture(customer);
	 }

	 @Override
	 public Customer getCustomerByEmail(String email) throws InterruptedException {
		  LOG.info("Filling the customer details for email {}", email);
		  Customer customer = new Customer();
		  customer.setFirstName("New");
		  customer.setLastName("Customer");
		  customer.setAge(30);
		  customer.setEmail("contact-us@javathread");
		  Thread.sleep(20000);
		  return customer;
	 }
	 
	 @Async
	 @Override
	 public CompletableFuture<List<Country>> getCountriesByLanguage(String language) {
		 LOG.info("Fetching Country Details By Language");
		 String url = "https://restcountries.com/v2/lang/" + language + "?fields=name";
		 Country[] response = restTemplate.getForObject(url, Country[].class);

		 return CompletableFuture.completedFuture(Arrays.asList(response));
	 }
	 
	 @Async
	 @Override
	 public CompletableFuture<List<Country>> getCountriesByRegion(String region) {
		 LOG.info("Fetching Country Details By Region");
		 String url = "https://restcountries.com/v2/region/" + region + "?fields=name";
		 Country[] response = restTemplate.getForObject(url, Country[].class);

		 return CompletableFuture.completedFuture(Arrays.asList(response));
	 }

}
