package com.example.ThreadSpringBoot;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class AsyncTaskDemo {

	@Async
	public void someAsyncMethod() {
		try {
			Thread.sleep(3000);	// Let me sleep for 3 sec
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("My Name " + Thread.currentThread().getName());
	}
}